package ru.bmstu.iu4

import java.io.FileReader
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.net.SocketException

import org.json.*

import kotlinx.coroutines.experimental.*

class TrafficJamRate {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            if (args.isEmpty()) {
                println("File was not selected")
                System.exit(-1)
            }
            else if(args.size > 1) {
                println("Why so many arguments?")
            }
            val filename = args[0]
            println("File name $filename")
            val text = FileReader(filename)
            val points = checkPoints(text.readLines())

            launchRequests(points)
        }


        private fun launchRequests(args: ArrayList<DoubleArray>) = runBlocking {

            val tasks = (0 until args.size).map { i ->
                async(CommonPool) {
                    getTrafficJamsOnTheRoute(args[i][0], args[i][1], args[i][2], args[i][3])
                }
            }

            tasks.forEach {
                println(it.await())
            }
        }


        private fun getTrafficJamsOnTheRoute(latA: Double, lngA: Double, latB: Double, lngB: Double): String {
            val inputStream: InputStream

            val path = "($latA, $lngA)->($latB, $lngB)"

            val url = URL("https://yandex.ru/geohelper/api/v1/router?points=$latA,$lngA~$latB,$lngB")
            val connection = url.openConnection() as HttpURLConnection

            connection.connect()

            try {
                inputStream = connection.inputStream
                inputStream.bufferedReader().use {
                    return "Пробки на маршруте $path: ${parseJson(it.readText())}"
                }
            }
            catch (e: SocketException) {
                return "Произошла ошибка при получении пробок на маршруте $path: ${e.message}"
            }
        }


        private fun parseJson(data: String): String {
            val json = JSONObject(data)
            return "${json["jamsRate"]} ${json["jamsMeasure"]}"
        }


        private fun checkPoints(lines: List<String>): ArrayList<DoubleArray> {

            var result = ArrayList<DoubleArray>()

            lines.forEachIndexed ({ k, line ->
                val values = line.split(" ")
                try {
                    val temp = values.map { it.toDouble() }
                    if (temp.isEmpty() || temp.size % 4.0 != 0.0) {
                        println("Ошибка в представлении маршрута №${k+1}")
                        System.exit(-1)
                    }
                    result.add(temp.toDoubleArray())
                }
                catch (pointNotDouble: Throwable) {
                    println("Координата маршрута №${k+1} повреждена")
                    System.exit(-1)
                }
            })
            return result
        }
    }
}
